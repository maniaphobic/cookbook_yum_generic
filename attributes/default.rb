# Default attributes

node.default['yum_artifactory'] = {
  'base_url'     => 'https://localhost/',
  'repositories' => {},
  'url_pattern'  => '%{scheme}://%{username}:%{password}@%{host_address}:%{port}%{base_path}/'
}
