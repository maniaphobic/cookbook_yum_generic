#
# Cookbook Name:: yum_artifactory
# Recipe:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

# Extract node attributes for convenience
attributes = node.fetch('yum_artifactory', {})

# Iterate over the repository definitions
attributes.fetch('repositories', {}).each do |repo_name, repo_defn|
  # If the repository requires authentication, fetch it from the
  # specified data bag item
  if repo_defn['authenticated?'] == true
    auth_creds = fetch_creds
    file_mode  = attributes.fetch('file_mode', '0600')
  else
    auth_creds = {}
    file_mode  = attributes.fetch('file_mode', '0644')
  end

  # Configure the repository
  yum_repository repo_name do
    action repo_defn.fetch('action'.to_sym, :create)
    baseurl repo_defn.fetch('baseurl')
    description repo_defn.fetch('description', repo_name)
    enabled repo_defn.fetch('enabled', true)
    gpgcheck repo_defn.fetch('gpgcheck', true)
    gpgkey repo_defn.fetch('gpgkey', nil)
    mode file_mode
    timeout repo_defn.fetch('timeout', 30)
  end
end
